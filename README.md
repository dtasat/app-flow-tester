# App Flow Tester

Este script permite ejecutar una prueba de flujo de una aplicación web a través de un código secuencial almacenado en un archivo `.aftscript` que contiene el paso-a-paso del flujo en comandos fáciles de entender con un mínimo conocimiento de `selectores css`.

https://www.freecodecamp.org/news/css-selectors-cheat-sheet/

## Instalación

Al descargar el proyecto se necesita tener instalado Node, luego ejecutar el comando `npm install` en la carpeta del proyecto.

## Forma de ejecución

Para ejecutar un archivo `.aftscript` abrimos una terminal en la home de este proyecto y ejecutamos:
- `ts-node src --file <ruta-al-archivo>`

Comandos adicionales:
- `--time-between-actions <time-in-ms>` permite definir una cierta cantidad de tiempo de espera entre cada una de las acciones ejecutadas.
- `--keep-open-after-finish` deja la pantalla del explorador abierta una vez finalizada la ejecución (solo si terminó correctamente).
- `--keep-open-on-error` deja la pantalla del explorador abierta si la ejecución terminó con errores.
- `--help` muestra los comandos disponibles.

## Sintaxis de los scripts

- Cargar una URL
  - Sintaxis: `get-url <url>`
  - Ejemplo: `get-url https://www.google.com/`
- Hacer click en un elemento (se puede utilizar en checkboxes, radio buttons, botones, etc)
  - Sintaxis: `click "<selector>"` donde `<selector>` es el selector css que apunta al elemento a hacer click
  - Ejemplo: `click ".login-button"`
- Completar una input de texto (o numérica)
  - Sintaxis: `fill-input "<selector>" "<texto>"` donde `<selector>` es el selector css de la input y `<texto>` es el texto a ingresar en la input.
  - Ejemplo 1: `fill-input ".username-input" "MiUsuario123"`
  - Ejemplo 2: `fill-input ".email-input" "miemail+<random-integer:4>@dominio.com"`
- Seleccionar una opción en un select
  - Sintaxis: `fill-select "<selector-al-select>" "<selector-a-opcion>"` donde `<selector-al-select>` apunta al elemento select de html, mientras que `<selector-a-opcion>` apunta al elemento option.
  - Ejemplo: `fill-select ".sex-selector" ".sex-selector option[name = "M"]"`
- Subir un archivo
  - Sintaxis: `upload-file "<selector>" "<ruta-al-archivo>"` donde `<selector>` es el selector css que apunta al input type="file", y `<ruta-al-archivo>` apunta al archivo que se desea subir.
  - Ejemplo: `upload-file ".file-input" "C:\\mi-archivo.csv"`
- Esperar a que aparezca un elemento en pantalla
  - Sintaxis: `wait-for-element "<selector>"` donde `<selector>` es el selector css al elemento que se desea esperar.
  - Ejemplo: `wait-for-element ".home"`
- Esperar sin hacer nada cierta cantidad de tiempo
  - Sintaxis: `sleep <tiempo-en-ms>`
  - Ejemplo: `sleep 1000`
- Cambiar a otra tab
  - Sintaxis: `switch-tab <tab-number>` donde `<tab-number>` es el número de tab empezando en cero.
  - Ejemplo: `switch-tab 1`
  - **Importante**: siempre dejar una sola tab abierta a la hora de terminar el script.
- Cerrar la tab actual
  - Sintaxis: `close-current-tab`.
  - **Importante**: nunca cerrar todas las tabs.
  - **Importante**: después de cerrar una tab se debe hacer un `switch-tab` a una tab existente.
- Declarar una variable como constante
  - Sintaxis: `var <nombre-de-variable> <valor>`
  - Ejemplo 1: `var id-usuario 123`
  - Ejemplo 2: `var id-usuario <random-number:3>`
- Declarar una variable desde un texto en pantalla
  - Sintaxis: `var-from-element <nombre-de-variable> <selector-al-elemento>` donde `<selector al elemento>` apunta al elemento que contiene el texto que desea que contenga la variable
  - Ejemplo: `var id-usuario span.user-id`
- Comentarios
  - Si la linea comienza con `#` será ignorada.

## Interpolación de textos

Todos los parámetros en todos los comandos pueden contenter ciertos textos que nos permiten dinamizarlos, integrando variables o valores aleatorios en ellos, estos son las opciones de interpolación disponibles:
- `<random-text:<longitud>>` permite ingresar un texto aleatorio de `<longitud>` palabras.
  - Ejemplo: `fill-input .nombre <random-text:3>`
- `<random-integer:<longitud>>` permite ingresar un número entero aleatorio de `<longitud>` dígitos.
  - Ejemplo: `fill-input .dni <random-number:8>`
- `<random-float:<longitud>>` permite ingresar un número decimal aleatorio de `<longitud>` dígitos enteros y dos decimales.
  - Ejemplo: `fill-input .monto-a-pagar <random-float:2>`
- `<today>` ingresa la fecha de hoy con estructura DD/MM/YYYY.
  - Ejemplo: `fill-input .fecha <today>`
- `<var:<nombre-de-variable>>` permite ingresar el valor actual de la variable `<nombre-de-variable>`.
  - Ejemplo: `fill-input .id-usuario <var:id-usuario>`


