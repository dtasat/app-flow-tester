import { By } from 'selenium-webdriver';
import * as fs from 'fs';
import { Executor } from './executor';
import randomWords from 'random-words';

export class ParsingError extends Error {}

export class Parser {
    
    executor = new Executor();

    // Modify here when adding an instruction
    parametersCount: { [key: string]: number } = {
        'get-url': 1,
        'click': 1,
        'fill-input': 2,
        'fill-select': 2,
        'upload-file': 2,
        'wait-for-element': 1,
        'sleep': 1,
        'switch-tab': 1,
        'close-current-tab': 0,
        'var-from-element': 2,
        'var': 2,
    };

    constructor() {}

    async runScript(fileName: string, timeBetweenActions: number) {
        const file = fs.readFileSync(fileName, 'utf-8');
        const commands = file.split('\n').map(c => c.trim());
        for (const command of commands) {
            if (!command.length || command.startsWith('#')) continue;
            const splittedCommand = this.splitCommand(command);
            if (splittedCommand?.length) {
                console.log('Ejecuntando instrucción', command)
                await this.executeInstruction(splittedCommand[0].trim(), splittedCommand.slice(1));
                if (timeBetweenActions) {
                    await this.executor.sleep(timeBetweenActions);
                }
            }
        }
    }

    splitCommand(command: string) {
        // Source: https://stackoverflow.com/questions/48142763/split-string-by-spaces-but-ignore-spaces-in-quotation-marks
        /* The regular expression. */
        const regex = /"[^"]+"|[^\s]+/g;
        /* Use 'map' and 'replace' to discard the surrounding quotation marks. */
        return (command.match(regex) ?? [command]).map(e => e.replace(/"(.+)"/, "$1"));
    }

    parseParams(params: string[]) {
        return params.map(param => {
            param = param.replace(/<random-text:\d+>/g, (match: string) => {
                const length = parseInt(match.match(/\d+/g)![0], 10);
                return this.generateRandomText(length);
            });
            param = param.replace(/<random-integer:\d+>/g, (match: string) => {
                const length = parseInt(match.match(/\d+/g)![0], 10);
                return Math.floor(this.generateRandomNumber(length)).toString();
            });
            param = param.replace(/<random-float:\d+>/g, (match: string) => {
                const length = parseInt(match.match(/\d+/g)![0], 10);
                return this.generateRandomNumber(length).toFixed(2);
            });
            param = param.replace(/<today>/g, (match: string) => {
                return new Date().toLocaleDateString('es-AR', { year: 'numeric', month: '2-digit', day: '2-digit' });
            });
            param = param.replace(/<var:[a-zA-Z_\-0-9]+>/g, (match: string) => {
                const varName = match.match(/[a-zA-Z_\-0-9]+/g)![1];
                return this.executor.getVar(varName);
            });
            return param;
        });
    }

    private generateRandomText(length: number) {
        return randomWords({ min: length, max: length, join: ' ' });
    }

    private generateRandomNumber(length: number) {
        return Math.pow(10, length-1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length-1) - 1);
    }

    async executeInstruction(instruction: string, params: string[]) {
        if (!(instruction in this.parametersCount)) {
            throw new ParsingError(`La instrucción ${instruction} no está disponible.`);
        }
        if (params.length !== this.parametersCount[instruction]) {
            throw new ParsingError(`La cantidad de parámetros no es correcta para la instrucción ${instruction}, deberían ser ${this.parametersCount[instruction]} y son ${params.length}.`);
        }
        params = this.parseParams(params);
        switch(instruction) {
            case 'get-url':
                await this.executor.get(params[0]);
                break;
            case 'click':
                await this.executor.click(By.css(params[0]));
                break;
            case 'fill-input':
                await this.executor.fillInput(By.css(params[0]), params[1]);
                break;
            case 'fill-select':
                await this.executor.fillSelect(By.css(params[0]), By.css(params[1]));
                break;
            case 'upload-file':
                await this.executor.fillFileInput(By.css(params[0]), params[1]);
                break;
            case 'wait-for-element':
                await this.executor.waitForElement(By.css(params[0]));
                break;
            case 'sleep':
                const ms = parseInt(params[0], 10);
                if (ms && !isNaN(ms)) {
                    await this.executor.sleep(ms);
                } else {
                    throw new ParsingError(`El parámetro ms no es un número entero`);
                }
                break;
            case 'switch-tab':
                await this.executor.switchTab(parseInt(params[0], 10));
                break;
            case 'close-current-tab':
                await this.executor.closeCurrentTab();
                break;
            case 'var-from-element':
                await this.executor.varFromElement(params[0], By.css(params[1]));
                break;
            case 'var':
                await this.executor.varFromConstant(params[0], params[1]);
                break;
            // Modify here when adding an instruction
        }
    }
}