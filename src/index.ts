import { ExecutionError } from './executor';
import { Parser, ParsingError } from './parser';
import * as yargs from 'yargs';

async function execute() {
    // Get parameters
    const args = await yargs
        .option('file', {
            description: 'Nombre del archivo .aftscript a ejecutar.',
            requiresArg: true,
            type: 'string'
        })
        .option('time-between-actions', {
            description: 'Delay entre instrucciones.',
            requiresArg: false,
            default: 0,
            type: 'number'
        })
        .option('keep-open-after-finish', {
            description: 'True si desea mantener abierto el explorador después que de la ejecución fuera completada correctamente.',
            requiresArg: false,
            default: false,
            type: 'boolean'
        })
        .option('keep-open-on-error', {
            description: 'True si desea mantener abierto el explorador después de un error.',
            requiresArg: false,
            default: false,
            type: 'boolean'
        })
        .help()
        .argv;
    let path = args.file as string;
    let timeBetweenActions = args.timeBetweenActions as number;
    let keepOpenAfterFinish = args.keepOpenAfterFinish as boolean;
    let keepOpenOnError = args.keepOpenOnError as boolean;
    
    if (!path) {
        console.error('El parámetro --file es requerido.');
        return;
    }

    // Execute
    const parser = new Parser();
    try {
        await parser.executor.initialize();
        await parser.runScript(path, timeBetweenActions);
        parser.executor.finalize(!keepOpenAfterFinish);
    } catch (error: any) {
        parser.executor.finalize(!keepOpenOnError);
        if (error instanceof ParsingError) {
            console.error('Error de parseo:', error.message);
        } else if (error instanceof ExecutionError) {
            console.error('Error de ejecución:', error.message);
        } else {
            console.error('Error no identificado', error.message);
        }
        return;
    }
}

execute();