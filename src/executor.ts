import { Builder, Locator, until, WebDriver } from 'selenium-webdriver';
import * as fs from 'fs';
import * as path from 'path';

require('chromedriver');

export class ExecutionError extends Error {}

export class Executor {
    driver?: WebDriver;
    vars: { [key: string]: string } = {};

    constructor() {}

    async initialize() {
        this.driver = await new Builder().forBrowser('chrome').build();
        await this.driver.manage().setTimeouts({
            implicit: 30000
        });
        console.log('############ Comenzando ejecución ############');
    }

    finalize(closeExplorer: boolean) {
        if (closeExplorer) {
            // We set a timeout before close becouse otherwise selenium sometines throws an invalid-session-id error.
            setTimeout(() => {
                this.driver!.close();
            }, 10);
        }
        console.log('############ Ejecución finalizada ############');
    }

    async get(url: string) {
        await this.driver!.get(url);
    }

    async click(locator: Locator) {
        const element = await this.getElement(locator);
        await this.driver!.wait(until.elementIsEnabled(element));
        await element.click();
    }

    async fillInput(locator: Locator, text: string) {
        const element = await this.getElement(locator);
        if (await element?.getTagName() === 'input') {
            element.sendKeys(text);
        } else {
            throw new ExecutionError(`El elemento: ${ locator.toString() } no es una input.`);
        }
    }

    async fillFileInput(locator: Locator, fileLocation: string) {
        if (fs.existsSync(fileLocation)) {
            const absoluteFileLocation = path.resolve(fileLocation);
            this.fillInput(locator, absoluteFileLocation);
        } else {
            throw new ExecutionError(`El archivo ${ fileLocation } no existe.`);
        }
    }

    async fillSelect(selectLocator: Locator, optionLocator: Locator) {
        await this.click(selectLocator);
        await this.click(optionLocator);
    }

    async waitForElement(locator: Locator) {
        await this.driver!.wait(until.elementLocated(locator));
    }

    async sleep(ms: number) {
        await this.driver!.sleep(ms);
    }

    async switchTab(tabNumber: number) {
        const handlers = await this.driver!.getAllWindowHandles();
        if (tabNumber < handlers.length) {
            this.driver!.switchTo().window(handlers[tabNumber]);
        } else {
            throw new ExecutionError(`Se quizo ir a la pestaña ${tabNumber} y hay ${handlers.length} pestañas abiertas.`);
        }
    }

    async closeCurrentTab() {
        await new Promise<void>(resolve => {
            setTimeout(() => {
                this.driver!.close();
                resolve();
            }, 10);
        });
    }
    
    async varFromElement(varName: string, locator: Locator) {
        const element = await this.getElement(locator);
        this.vars[varName] = await element.getText();
    }
    
    async varFromConstant(varName: string, constant: string) {
        this.vars[varName] = constant;
    }

    getVar(varName: string) {
        return this.vars[varName] ?? '';
    }

    // Modify here when adding an instruction

    private async getElement(locator: Locator) {
        try {
            const element = await this.driver!.findElement(locator);
            return element;
        } catch (error: any) {
            throw new ExecutionError(`Error al localizar elemento: ${ locator.toString() }. Mensaje de error: ${ error.message }`);
        }
    }
}